/*******************************************************************************
 * Laboration 3, Kurs: DT161G
 * File: main.js
 * Desc: main JavaScript file for Laboration 3
 *
 * Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 ******************************************************************************/


var xhr;                // Variabel att lagra XMLHttpRequestobjektet
var loginBtn = byId("login");
var logoutBtn = byId("logout");

/*******************************************************************************
 * Util functions
 ******************************************************************************/
function byId(id) {
    return document.getElementById(id);
}

/******************************************************************************/


/*******************************************************************************
 * Main function
 ******************************************************************************/
function main() {
    clearErrors();
    var loginButton = byId("loginButton");
    var logoutButton = byId("logoutButton");
    if (loginButton !== null) {
        byId("loginButton").addEventListener('click', doLogin, false);
    }
    if (logoutButton !== null) {
        byId("logoutButton").addEventListener('click', doLogout, false);
    }
    // Stöd för IE7+, Firefox, Chrome, Opera, Safari
    try {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xhr = new XMLHttpRequest();
        }
        else if (window.ActiveXObject) {
            // code for IE6, IE5
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else {
            throw new Error('Cannot create XMLHttpRequest object');
        }

    } catch (e) {
        alert('"XMLHttpRequest failed!' + e.message);
    }
}

window.addEventListener("load", main, false); // Connect the main function to window load event

/*******************************************************************************
 * Function doLogin
 ******************************************************************************/
function doLogin() {
    if (byId('uname').value !== "" && byId('psw').value !== "") {
        var username = encodeURIComponent(byId('uname').value);
        var password = encodeURIComponent(byId('psw').value);

        xhr.addEventListener('readystatechange', processLogin, false);
        xhr.open('POST', 'login.php', true);
        xhr.setRequestHeader("Content-type", 'application/x-www-form-urlencoded');
        xhr.send("username=" + username + "&password=" + password);
    }
    else {
        if (byId('uname').value === "") {
            byId("usernameError").style.display = "block";
            byId("usernameError").innerHTML = "The username cannot be empty";
        }
        if (byId('psw').value === "") {
            byId("passwordError").style.display = "block";
            byId("passwordError").innerHTML = "The password cannot be empty";
        }
    }
}

/*******************************************************************************
 * Function doLogout
 ******************************************************************************/
function doLogout() {
    xhr.addEventListener('readystatechange', processLogout, false);
    xhr.open('GET', 'logout.php', true);
    xhr.send(null);
}

/*******************************************************************************
 * Function processLogin
 ******************************************************************************/
function processLogin() {
    if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
        //First we must remove the registered event since we use the same xhr object for login and logout
        xhr.removeEventListener('readystatechange', processLogin, false);
        var response = JSON.parse(this.responseText);
        console.log(response);
        if (response.loggedIn) {
            var good = byId("success");
            good.style.display = "block";
            good.innerHTML = response.successMessage;
            if (logoutBtn !== null) {
                byId('logout').style.display = "block";
            }
            if (loginBtn !== null) {
                byId('login').style.display = "none";
            }

            location.reload();
        }
        else { // process errors
            for (var val in response) {
                if (val.startsWith("user")) {
                    var uname = byId("usernameError");
                    uname.style.display = "block";
                    uname.innerHTML = response[val] + "<br>";
                }
                if (val.startsWith("pass")) {
                    var pass = byId("passwordError");
                    pass.style.display = "block";
                    pass.innerHTML = response[val] + "<br>";
                }
            }
        }
    }
}

/*******************************************************************************
 * Function processLogout
 ******************************************************************************/
function processLogout() {
    if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
        //First we most remove the registered event since we use the same xhr object for login and logout
        xhr.removeEventListener('readystatechange', processLogout, false);
        var myResponse = JSON.parse(this.responseText);
        var good = byId("success");
        good.style.display = "block";
        good.innerHTML = myResponse;
        if (loginBtn !== null) {
            byId('login').style.display = "block";
        }
        if (logoutBtn !== null) {
            byId('logout').style.display = "none";
        }
        location.reload();
    }
}

/**
 * Clear username errors when user starts to enter a new username.
 * Clear password error when user starts to enter the password
 */
function clearErrors() {
    var uname = byId("uname");
    var psw = byId("psw");
    if (uname !== null) {
        byId("uname").addEventListener("input", function () {
            byId("usernameError").innerHTML = "";
            byId("usernameError").style.display = "none";
        }, false);
    }
    if (psw !== null) {
        byId("psw").addEventListener("input", function () {
            byId("passwordError").innerHTML = "";
            byId("passwordError").style.display = "none";
        }, false);
    }
}