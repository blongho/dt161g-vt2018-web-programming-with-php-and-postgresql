<?php
include_once "util.php";
checkSession();
/*******************************************************************************
 * Laboration 4, Kurs: DT161G
 * File: admin.php
 * Desc: Admin page for laboration 4
 *
 * Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 ******************************************************************************/
// Här skall det ske kontroll om man har loggat in och är behörig att se denna sida.
// Ni måste också kontrollera att användaren har rollen admin för att få se denna sida
// Annars redirekt till starsidan
$title = "Laboration 4";
$isUserLoggedIn = (isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"] === true);
$user = "";
$links = Config::getInstance()->get("links")["default"];
if (!isset($_SESSION["user"])){
	header("Location: index.php"); /* Redirect browser */
	exit;
}else{
	$user = $_SESSION["user"];
	$role = $_SESSION["role"];
	if ($user === "member"){
		header("Location: members.php");
		exit;
	}else{
		$links = $_SESSION["links"];
	}
}
?>
<!DOCTYPE html>
<html lang="sv-SE">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DT161G-Laboration4-admin</title>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="js/main.js"></script>
</head>
<body>
<header>
    <img src="img/mittuniversitetet.jpg" alt="miun logga" class="logo"/>
    <h1><?php echo $title ?></h1>
</header>
<main>
    <aside>
        <div id="authenticate">
            <div id="login">
				<?php if (!$isUserLoggedIn): ?>
                <h2>LOGIN</h2>
                <form id="loginForm">
                    <label for="uname">Username
                        <input type="text" placeholder="m" name="uname"
                               id="uname"
                               maxlength="10" value="m" autocomplete="off">
                    </label>
                    <label for="psw">Password
                        <input type="password" placeholder="Enter Password"
                               name="psw"
                               id="psw"></label>
                    <label for="loginButton">
                        <button type="button" id="loginButton" name="login">
                            Login
                        </button>
                    </label>
                </form>
            </div>
            <div id="logout">
				<?php else: ?>
                    <h2>LOGOUT</h2>
                    <button type="button" id="logoutButton" name="logout">
                        Logout
                    </button>
				<?php endif; ?>
            </div>
        </div>
        <h2>MENY</h2>
		<?php displayLinks($links); ?>
        <div id="authenticationFeedback">
            <p id="success" class="green hide"></p>
            <p id="usernameError" class="hide red"></p>
            <p id="passwordError" class="hide red"></p>
        </div>
    </aside>
    <section>
        <h2>Adminsida</h2>
        <p>Denna sida skall bara kunna ses av inloggade medlemar,<br>
            som har administratörsrättigheter.</p>
		<?php if ($isUserLoggedIn): ?>
            <p>You are logged in as '<b><?php echo $_SESSION["user"] . " ["
			                                       . $_SESSION["role"] . "]";
					?></b>'
            </p>
		<?php endif; ?>
    </section>
</main>
<footer>
	<?php printFooter(); ?>
</footer>
</body>
</html>
