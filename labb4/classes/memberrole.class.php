<?php
/**
 * Created by PhpStorm.
 * User: blongho
 * Date: 2018-08-08
 * Time: 18:41
 */

class MemberRole extends Role
{
	/**
	 * MemberRole constructor.
	 */
	public function __construct()
	{
		parent::__construct("member", "Member of the organization");
	}
}