<?php

/*******************************************************************************
 * Laboration 4, Kurs: DT161G
 * File: message.class.php
 * Desc: Class Message for laboration 4.
 * This is class represents a message
 *
 * @author Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 *
 * @since 2018-08-06
 ******************************************************************************/
class Message
{

	private $name = "";
	private $text = "";
	private $ip = "";
	private $time = "";

	/**
	 * Message constructor.
	 *
	 * @param string $name the writer's name
	 * @param string $text the text entered
	 * @param null   $ip
	 * @param null   $time
	 */
	public function __construct(
		string $name,
		string $text,
		$ip = null,
		$time = null
	) {
		$this->name = sanitize($name);
		$this->text = sanitize($text);
		$this->ip = sanitize(($ip === null ? $_SERVER["REMOTE_ADDR"] : $ip));
		$this->time = sanitize(
			($time === null ? date("Y-m-d H:i", time()) : $time)
		);
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getText(): string
	{
		return $this->text;
	}

	/**
	 * @param string $text
	 */
	public function setText(string $text): void
	{
		$this->text = $text;
	}

	/**
	 * @return string the ip address
	 */
	public function getIp(): string
	{
		return $this->ip;
	}

	/**
	 * @param string $ip
	 */
	public function setIp(string $ip): void
	{
		$this->ip = $ip;
	}

	/**
	 * @return null
	 */
	public function getTime()
	{
		return $this->time;
	}

	/**
	 * @param string $time
	 */
	public function setTime(string $time): void
	{
		$this->time = $time;
	}

	/**
	 * Display table on table row
	 */
	public function displayAsRow(): void
	{
		echo "<tr><td>$this->name</td>"
		     ."<td>$this->text</td>"
		     ."<td>IP: $this->ip<br>TID: $this->time</td></tr>";
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return "Writer: $this->name<br>Text: $this->text<br>IP: $this->ip<br>Time: $this->time<br>";
	}


}