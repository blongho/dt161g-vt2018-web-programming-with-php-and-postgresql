<?php
/*******************************************************************************
 * Laboration 4, Kurs: DT161G
 * File: database.class.php
 * Desc: Class Database for laboration 4.
 * This is a singleton class that makes and closes connections to the database
 *
 * @author Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 ******************************************************************************/

class Database
{

	private static $instance; // the instance of this class

	private static $connectionString;

	/**
	 * Try to get the connection string
	 *
	 * @return string the connection string for a database or null if the file
	 *                cannot be read or any of the connection string values is
	 *                missing
	 */
	public static function getConnectionString(): string
	{
		$localDb = Config::getInstance()->get("lobe1602");
		$str = null;

		if ( ! empty($localDb)) {
			$dbname = $localDb["dbname"];
			$host = $localDb["host"];
			$user = $localDb["user"];
			$pwd = $localDb["password"];
			$port = $localDb["port"];
			if ($host !== null && $port !== null && $dbname !== null
			    && $user !== null
			    && $pwd !== null) {

				$str
					= "host=$host port=$port dbname=$dbname user=$user password=$pwd";
			}
		}

		return $str;
	} // the database connection string

	/**
	 * Make class private to prevent instantiation elsewhere
	 * Database constructor.
	 */
	private function __construct()
	{
		self::$connectionString = self::getConnectionString();
	}


	/**
	 * Returns the current instance if one exists,
	 * otherwise creates it.
	 *
	 * @return Database The single instance of this class.
	 */
	public static function getInstance()
	{
		if ( ! self::$instance) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Opens connection to PostgreSQL database.
	 *
	 * @return resource The connection to the server if it succeeded.
	 */
	private function connect()
	{
		if (self::$connectionString !== null) {
			$dbconn = pg_connect(self::$connectionString);

			return $dbconn;
		}

		return null;
	}

	/**
	 * Closes the database PostgreSQL connection.
	 *
	 * @param resource The connection to be closed.
	 */
	private function disconnect($dbconn)
	{
		pg_close($dbconn);
	}


	/**
	 * Executes a simple query without parameters.
	 *
	 * @param $queryString string The query to be asked.
	 *
	 * @return null|resource A result set or false = failed to connect or empty
	 * result.
	 */
	public function doSimpleQuery($queryString)
	{
		$dbconn = $this->connect();

		if ($dbconn) {
			$result = pg_query($dbconn, $queryString);

			$this->disconnect($dbconn);

			return $result;
		}

		return null;
	}

	/**
	 * Queries the PostgreSQL database with a parameterized string.
	 *
	 * @param $queryString string The query string.
	 * @param $params      array The parameters for the string.
	 *
	 * @return null|resource A result set or false = failed to connect or empty
	 * result.
	 */
	public function doParamQuery($queryString, $params)
	{
		$dbconn = $this->connect();

		if ($dbconn) {
			$result = pg_query_params($dbconn, $queryString, $params);

			$this->disconnect($dbconn);

			return $result;
		}

		return null;
	}

}