<?php
/**
 * Created by PhpStorm.
 * User: blongho
 * Date: 2018-08-08
 * Time: 17:08
 */

class Admin extends User
{

	public function __construct(string $username, string $password)
	{
		parent::__construct($username, $password, new AdminRole());
	}

}