<?php
/**
 * Created by PhpStorm.
 * User: blongho
 * Date: 2018-08-08
 * Time: 18:39
 */

class AdminRole extends Role
{
	/**
	 * AdminRole constructor.
	 */
	public function __construct()
	{
		parent::__construct("admin", "Administrator of the organization");
	}
}