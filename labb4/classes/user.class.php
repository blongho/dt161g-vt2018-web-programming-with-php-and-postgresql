<?php
/**
 * Created by PhpStorm.
 * User: blongho
 * Date: 2018-08-08
 * Time: 17:17
 */

abstract class User
{

	private $roles = [];
	private $username = "";
	private $password = "";

	/**
	 * User constructor.
	 *
	 * @param string     $username
	 * @param string     $password
	 * @param \Role|null $role
	 */
	public function __construct(
		string $username,
		string $password,
		Role $role = null
	) {
		$this->username = $username;
		$this->password = $password;
		$this->roles[] = new MemberRole();
		if ($role !== null) {
			array_push($this->roles, $role);
		}
	}

	/**
	 * @return string
	 */
	public function getUsername(): string
	{
		return $this->username;
	}

	/**
	 * @param string $username
	 */
	public function setUsername(string $username): void
	{
		$this->username = $username;
	}

	/**
	 * @return string
	 */
	public function getPassword(): string
	{
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword(string $password): void
	{
		$this->password = $password;
	}

	/**
	 * Get the roles for a user
	 *
	 * @return string
	 */
	public function getRoles(): string
	{
		$roleStr = "Roles: ".$this->getRoleCount().": ";
		if ( ! empty($this->roles)) {
			foreach ($this->roles as $role) {
				$roleStr .= $role->getRole();
				$roleStr .= "<br>";
			}
		}

		return $roleStr;
	}

	/**
	 * Get the number of roles that this user has
	 *
	 * @return int
	 */
	public function getRoleCount(): int
	{
		return count($this->roles);
	}

	/**
	 * @param \Role $roles
	 */
	public function setRole(Role $role): bool
	{
		if ($role !== null) {
			if ($this instanceof Member && count($this->roles) < 1) {
				$this->roles[] = $role;

				return true;
			} else if ($this instanceof Admin && count($this->roles) < 2) {
				$this->roles[] = $role;

				return true;
			}
		}

		return false;
	}

	/**
	 * Check if a user is an admin
	 *
	 * @return bool true if one of the roles is an admin
	 */
	public function isAdmin(): bool
	{
		foreach ($this->roles as $role) {
			if ($role->getRole() === "admin") {
				return true;
			}
		}

		return false;
	}

	/**
	 * The __toString method allows a class to decide how it will react when it
	 * is converted to a string.
	 *
	 * @return string
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.tostring
	 */
	public function __toString()
	{
		$str = __CLASS__
		       ."<br>Username: $this->username<br>Password: $this->password<br> ";
		$str .= $this->getRoles();

		return $str;
	}


}