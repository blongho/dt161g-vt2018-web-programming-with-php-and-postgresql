<?php

/*******************************************************************************
 * Laboration 4, Kurs: DT161G
 * File: dbmanager.class.php
 * Desc: Class DbManager for laboration 4.
 * This class manages communication with the database, all updates on the
 * database are done here
 *
 * @author Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 * @since  2018-08-09
 ******************************************************************************/
class DbManager{

	/**
	 * DbManager constructor is empty. Let the heavy lifting be done by the
	 * respective functions
	 */
	public function __construct(){
	}

	/**
	 * Post a message to the database
	 *
	 * @param Message $msg the message
	 *
	 * @return array an array with key-value messages. Keys failed|success as
	 *               the case might be
	 */
	public static function postMessage(Message $msg): array{
		$feedback = [];
		if ($msg !== null){
			$name = escapestring($msg->getName());
			$query = "INSERT INTO dt161g.guestbook"
			         . " ( name, message, iplog, timelog) "
			         . "VALUES ($1, $2, $3, $4);";

			$db = Database::getInstance();

			$results = $db->doParamQuery($query, (array) $msg);

			if ($results !== null){
				pg_free_result($results);
			}else{
				$feedback["failed"]
					= "Dear <b>'$name'</b>, your message could not be posted";
			}

		}else{
			$feedback["failed"] = "Attempt to post empty message refused!";
		}

		return $feedback;
	}

	/**
	 * Get all messages in the database
	 *
	 * @return array all database messages
	 */
	public static function getAllMessages(): array{
		$query = "SELECT * FROM dt161g.guestbook"
		         . " ORDER BY timelog ASC;";
		$db = Database::getInstance();
		$results = $db->doSimpleQuery($query);
		$messages = [];
		if (!empty($results)){
			while ($msg = pg_fetch_row($results)){
				$messages [] = new Message($msg[1], $msg[2], $msg[3], $msg[4]);
			}
			pg_free_result($results);

		}

		return $messages;
	}


	/**
	 * Check if a username exists
	 *
	 * @param string $username the username to look for
	 *
	 * @return bool true if username exists, otherwise false
	 */
	public static function usernameExists(string $username): bool{
		$uname = escapestring($username);
		$query = "SELECT username FROM dt161g.member"
		         . " WHERE username = $1;";
		$db = Database::getInstance();
		$results = $db->doParamQuery($query, [$uname]);
		if (pg_affected_rows($results) > 0){
			pg_free_result($results);

			return true;
		}

		return false;
	}


	/**
	 * Verify a user by checking the username and password that they match in
	 * the database
	 *
	 * @param string $username the username of the user
	 * @param string $password the password of the user
	 *
	 * @return bool true is username and password match otherwise false
	 */
	public static function isValidMember(string $username, string $password):
	bool{
		$uname = escapestring($username);
		$psw = escapestring($password);
		$query = "SELECT username FROM dt161g.member"
		         . " WHERE username = $1 AND password = $2;";
		$db = Database::getInstance();
		$results = $db->doParamQuery($query, [$uname, $psw]);
		if (pg_affected_rows($results) > 0){ // this is a valid user,
			pg_free_result($results);

			return true;
		}

		return false;
	}

	/**
	 * Get a user from the database using the password and username
	 * To use this function, first call isValid user
	 *
	 * @param string $username username
	 * @param string $password password
	 *
	 * @return array user attributes in an array
	 */
	public static function getUser(string $username, string $password): array{
		$user = [];
		$query
			= "SELECT role FROM dt161g.role "
			  . "WHERE id IN (SELECT role_id FROM dt161g.member_role "
			  . "WHERE member_id = (SELECT id FROM dt161g.member WHERE username = $1));";

		$db = Database::getInstance();
		$uname = escapestring($username);
		$psw = escapestring($password);
		$results = $db->doParamQuery($query, [$uname]);
		if (pg_affected_rows($results) > 0){
			$values = [];
			while ($row = pg_fetch_row($results)){
				$values[] = $row[0];
			}
			if (in_array("admin", $values)){
				$usr = new Admin($uname, $psw);
			}else{
				$usr = new Member($uname, $psw);
			}
		}
		if ($user !== null){
			$user["username"] = $usr->getUsername();
			$user["password"] = $usr->getPassword();
			$user["role"] = $usr->isAdmin() ? "admin" : "member";
		}

		return $user;
	}

	/**
	 * Check if a user has already filled in the guestbook.
	 *
	 * @param string $ipAddress the ip address of the user from the database
	 *
	 * @return bool true if that ip has already been logged otherwise false
	 */
	public static function isUserHasFilledForm(string $ipAddress): bool{
		if ($ipAddress !== null){
			$query = "SELECT iplog FROM dt161g.guestbook WHERE iplog = $1";
			$results = Database::getInstance()->doParamQuery($query, array($ipAddress));

			return pg_affected_rows($results) > 0;
		}

		return false;
	}
}