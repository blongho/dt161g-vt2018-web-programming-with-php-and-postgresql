<?php

/*******************************************************************************
 * Laboration 3, Kurs: DT161G
 * File: config.class.php
 * Desc: Class Config for laboration 3.
 * This is a singleton class that reads the config file and gets the contents
 *
 * @author Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 * @since  2018-08-05
 ******************************************************************************/
class Config
{

	/**<Information about a database */
	private $data;

	/**Instance of this class */
	private static $instance;


	/**
	 * Make private to avoid more than one instance of the same class
	 * Config constructor.
	 */
	private function __construct()
	{
		$this->data = $this->readFile();
	}

	/**
	 * Read the config file and return its content or null object
	 *
	 * @return array
	 */
	private function readFile(): array
	{
		$content = [];
		$configFile = file_get_contents("app/config.json");
		if ($configFile) {
			$content = json_decode($configFile, true);
		}

		return $content;
	}

	/**
	 * Disallow cloning of the class
	 */
	private function __clone()
	{
	}

	/**
	 * Get an instance of this class
	 *
	 * @return Config this class
	 */
	public static function getInstance()
	{
		if ( ! self::$instance) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Get the values required for a particular configuration set in the config
	 * file
	 *
	 * @param $key string name for the array
	 *
	 * @return array null if $key is not in the config file or an associated
	 *               array of
	 *             <br/>the values of the array
	 */
	public function get(string $key): array
	{
		$arr = [];
		if ( ! empty($this->data)) {
			$arr = $this->data[$key];
		}

		return $arr;
	}

	/**
	 * reset the instance to null when config file is finished
	 */
	public function __destruct()
	{
		self::$instance = null;
	}
}