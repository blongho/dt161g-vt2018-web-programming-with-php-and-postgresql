<?php
/*******************************************************************************
 * Laboration 4, Kurs: DT161G
 * File: member.class.php
 * Desc: Class Member for laboration 4
 *
 * @author Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 * @since  2018-08-08
 ******************************************************************************/


class Member extends User
{

	/**
	 * Member constructor.
	 *
	 * @param string $username
	 * @param string $password
	 */
	public function __construct(string $username, string $password)
	{
		parent::__construct($username, $password);
	}

}