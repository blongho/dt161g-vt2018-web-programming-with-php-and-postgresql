<?php
/*******************************************************************************
 * Laboration 4, Kurs: DT161G
 * File: role.class.php
 * Desc: Class Role for laboration 4
 *
 * @author Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 * @since  2018-08-08
 ******************************************************************************/


abstract class Role
{

	private $role = "";
	private $roletext = "";

	/**
	 * Role constructor.
	 *
	 * @param string $role
	 * @param string $roletext
	 */
	public function __construct(string $role, string $roletext)
	{
		$this->role = $role;
		$this->roletext = $roletext;
	}

	/**
	 * @return string
	 */
	public function getRole(): string
	{
		return $this->role;
	}

	/**
	 * @param string $role
	 */
	public function setRole(string $role): void
	{
		$this->role = $role;
	}

	/**
	 * @return string
	 */
	public function getRoletext(): string
	{
		return $this->roletext;
	}

	/**
	 * @param string $roletext
	 */
	public function setRoletext(string $roletext): void
	{
		$this->roletext = $roletext;
	}

	/**
	 * The __toString method allows a class to decide how it will react when it
	 * is converted to a string.
	 *
	 * @return string
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.tostring
	 */
	public function __toString()
	{
		return "[".__CLASS__
		       ."]<br>Role: $this->role<br>Description: $this->roletext<br>";
	}


}