<?php
/**
 * @file functions.php
 * Created by PhpStorm.
 * User: lobe1602, Bernard Che Longho
 * Date: 8/2/2018
 * Time: 8:10 PM
 * This file contains some global functions that will be used across the project
 */

/**
 * Print the footer for html pages
 */
function printFooter(){
	echo "<p>Brought to you by <a href='mailto:lobe1602@student.miun.se'>Bernard Che 
Longho</a></p>";
}

/**
 * Remove all special characters that could be submitted in a form
 *
 * @see Source: http://php.net/manual/en/function.htmlspecialchars.php#101592
 *
 * @param string $text text to be sanitized the form
 *
 * @return null|string|string[] formatted text with no tags
 */
function sanitize(string $text): string{
	$text = htmlspecialchars($text);
	$text = preg_replace("/=/", "=\"\"", $text);
	$text = preg_replace("/&quot;/", "&quot;\"", $text);
	$tags
		= "/&lt;(\/|)(\w*)(\ |)(\w*)([\\\=]*)(?|(\")\"&quot;\"|)(?|(.*)?&quot;(\")|)([\ ]?)(\/|)&gt;/i";
	$replacement = "<$1$2$3$4$5$6$7$8$9$10>";
	$text = preg_replace($tags, $replacement, $text);
	$text = preg_replace("/=\"\"/", "=", $text);
	$text = strip_tags($text, ''); // remove extra tags

	return $text;
}

/**
 * Check if this is the same person trying to make an entry
 *
 * @return bool True if same IP or same computer
 */
function isSamePerson(): bool{
	if (isset($_COOKIE["IP"]) && ($_COOKIE["IP"] === $_SERVER["REMOTE_ADDR"])){
		return true;
	}

	return false;
}

/**
 * Display the links of a particular page.
 *
 * @param array $links the array of links (key-pair)
 */
function displayLinks(array $links){
	if (!empty($links)){
		echo "<nav id='links'> <ul>";
		foreach ($links as $key => $value){
			echo "<li><a href='$value'>" . ucfirst($key) . "</a></li>";
		}
		echo "</ul></nav>";
	}
}

/**
 * Escape string for non-sql characters
 *
 * @param string $string the string to be entered into the database
 *
 * @return string the escaped string
 */
function escapestring(string $string): string{
	return pg_escape_string($string);
}


/**
 * Display the messages from the database. If no message is returned, do not
 * display any table
 *
 * @param array $messages messages read from the database
 */
function printMessagesFromDb(array $messages){
	if (empty($messages)){
		echo "<p>Please fill in the guest book</p>";
	}else{
		echo "<table><tr><th class='th20'>FRÅN</th>"
		     . "<th class='th40'>INLÄGG</th>" .
		     "<th class='th40'>LOGGNING</th></tr>";
		foreach ($messages as $message){
			echo "<tr><td>" . $message->getName() . "</td>"
			     . "<td>" . $message->getText() . "</td>"
			     . "<td>IP: " . $message->getIp() . "<br>TID: " . $message->getTime()
			     . "</td></tr>";
		}
		echo "</table>";
	}
}

/**
 * Check the session whether it is overdue (more than 15 minutes) and destroy it and redirect to
 * homepage
 */
function checkSession(){
	if (isset($_SESSION["expire"])){
		$now = time();
		if ($now > $_SESSION["expire"]){
			session_destroy();
			//header("Location: http://hydra.miun.se/~lobe1602/dt161g/labb4/");
		}
	}
}