<?php
include_once "util.php";
/**
 * NB: Run this file just once. It drops the database and creates a new one with the initial
 * values set in sql/labb4.sql
 *
 *
 * Created by PhpStorm.
 * User: Bernard Che Longho (lobe1602)
 * Date: 8/10/2018
 * Time: 3:30 PM
 */

$query = "
-- ##############################################
-- First we create the member table
-- ##############################################
DROP TABLE IF EXISTS dt161g.member;

CREATE TABLE dt161g.member (
  id        SERIAL PRIMARY KEY,
  username  text NOT NULL CHECK (username <> ''),
  password  text NOT NULL CHECK (password  <> ''),
  CONSTRAINT unique_user UNIQUE(username)
)
WITHOUT OIDS;

-- ##############################################
-- Now we insert some values
-- ##############################################
INSERT INTO dt161g.member (username, password) VALUES ('m','m');
INSERT INTO dt161g.member (username, password) VALUES ('a','a');

-- ##############################################
-- Then we create the role table
-- ##############################################
DROP TABLE IF EXISTS dt161g.role;

CREATE TABLE dt161g.role (
  id        SERIAL PRIMARY KEY,
  role      text NOT NULL CHECK (role <> ''),
  roletext  text NOT NULL CHECK (roletext <> ''),
  CONSTRAINT unique_role UNIQUE(role)
)
WITHOUT OIDS;

-- ##############################################
-- Now we insert some values
-- ##############################################
INSERT INTO dt161g.role (role, roletext) VALUES ('member','Meddlem i föreningen');
INSERT INTO dt161g.role (role, roletext) VALUES ('admin','Administratör i föreningen');

-- ##############################################
-- Then we create the role table
-- ##############################################
DROP TABLE IF EXISTS dt161g.member_role;

CREATE TABLE dt161g.member_role (
  id        SERIAL PRIMARY KEY,
  member_id integer REFERENCES dt161g.member (id),
  role_id   integer REFERENCES dt161g.role (id),
  CONSTRAINT unique_member_role UNIQUE(member_id, role_id)
)
WITHOUT OIDS;

-- ##############################################
-- Now we insert some values
-- ##############################################
INSERT INTO dt161g.member_role (member_id, role_id) VALUES (1,1);
INSERT INTO dt161g.member_role (member_id, role_id) VALUES (2,1);
INSERT INTO dt161g.member_role (member_id, role_id) VALUES (2,2);
";

$db = Database::getInstance();
$db->doSimpleQuery($query);