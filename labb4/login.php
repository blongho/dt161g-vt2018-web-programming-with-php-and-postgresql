<?PHP
session_start();
include_once "util.php";
/*******************************************************************************
 * Laboration 4, Kurs: DT161G
 * File: login.php
 * Desc: Login page for laboration 4
 *
 * Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 ******************************************************************************/

/**
 * @var $link_array  array holds the links to be displayed when a user has
 * logged in
 * */
$link_array = Config::getInstance()->get("links")["default"];

/**
 * Set session expiration to 15 minutes from login
 */
define("FIFTEEN_MINUTES", 15 * 60);

$username = "";
$password = "";
$usernameHasValue = false;
$passwordHasValue = false;
$rememberMe = false;
$feedback = []; // array to hold the feedback information
$isSuccessfulLogin = false;

if (strtoupper($_SERVER['REQUEST_METHOD']) === 'POST'){

	// process user name and set errors if there exist
	if (isset($_POST["username"])){
		$username = $_POST["username"];
		if (!empty($username)){
			$username = sanitize($username);
			$usernameHasValue = true;
		}else{
			$feedback["userNameEmpty"] = "User name cannot be empty";
		}
	}else{
		$feedback["userNameNotSet"] = "Could not process username value";
	}

	// Process password and set errors is there exist
	if (isset($_POST["password"])){
		$password = $_POST["password"];
		if (!empty($password)){
			$password = sanitize($password);
			$passwordHasValue = true;
		}else{
			$feedback["passwordEmpty"] = "Password cannot be empty";
		}
	}else{
		$feedback["passwordNotSet"] = "Could not process the password value";
	}

	/**
	 * Verify a user credentials
	 * If the username and password are both not empty
	 *      Check if the username exists
	 *          If the username exists, check if the password match
	 *              If password match, do login stuff and authenticate user
	 */
	if ($passwordHasValue && $usernameHasValue){
		if (DbManager::usernameExists($username)){ // if user exists
			if (DbManager::isValidMember($username, $password)){ // verify user
				$member = DbManager::getUser($username, $password);
				$_SESSION["user"] = $username;
				$_SESSION["loggedIn"] = true;
				$member_links = Config::getInstance()->get("links")["member"];

				$link_array = array_merge($link_array, $member_links);

				$_SESSION["links"] = $link_array;
				$feedback["links"] = $link_array;
				$feedback["loggedIn"] = $_SESSION["loggedIn"];
				$feedback["successMessage"]
					= "You successfully logged in as '$username'";
				$isSuccessfulLogin = true;
				$_SESSION["start"] = time();
				$_SESSION["expire"] = $_SESSION["start"] +
				                      Config::getInstance()->get(
					                      "session"
				                      )["expire"];

				$role = $member["role"];
				$_SESSION["role"] = $role;
				if ($role === "admin"){

					$adminLinks = Config::getInstance()->get("links")["admin"];

					$link_array = array_merge($link_array, $adminLinks);

					$_SESSION["links"] = $link_array;
				}

			}else{ // password mismatch
				$feedback["passwordMismatch"]
					= "The password provided does not match";
			}
		}else{ // no user present
			$feedback["userAbsent"]
				= "No user with the username '$username' in our system'";
		}

	}else{
		$_SESSION["loggedIn"] = false;
		$feedback["success"] = false;
	}

	header('Content-Type: application/json; charset=utf-8');
	echo json_encode($feedback);
}else{ // nothing posted, redirect to home page
	header("Location: http://hydra.miun.se/~lobe1602/dt161g/labb4/");
}


