# DT161G VT 2018 Web programming with PHP and PostgreSQL #

### What is this repository for? ###
This respository is for my laboratory work and project for the 
above-mensioned course.

* Version 1.0 

### Contribution guidelines ###
No external contributions are required since this course requires that 
we submit **individual work**. 

### Who do I talk to? ###
For any enquiry, contact me at <blongho02@gmail.com>  
or my student email at <lobe1602@student.miun.se> 