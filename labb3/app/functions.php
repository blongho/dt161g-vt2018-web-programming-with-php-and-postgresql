<?php
/**
 * @file functions.php
 * Created by PhpStorm.
 * User: lobe1602, Bernard Che Longho
 * Date: 8/2/2018
 * Time: 8:10 PM
 * This file contains some global functions that will be used across the project
 */

/**
 * Print the footer at the end of the page
 */
function printFooter(){
	echo "<p>Brought to you by <a href='mailto:lobe1602@student.miun.se'>Bernard Che Longho</a></p>";
}

/**
 * Remove all special characters that could be submitted in a form
 *
 * @see Source: http://php.net/manual/en/function.htmlspecialchars.php#101592
 *
 * @param string $text text to be sanitized the form
 *
 * @return null|string|string[] formatted text with no tags
 */
function sanitize(string $text): string{
	$text = htmlspecialchars($text);
	$text = preg_replace("/=/", "=\"\"", $text);
	$text = preg_replace("/&quot;/", "&quot;\"", $text);
	$tags
		= "/&lt;(\/|)(\w*)(\ |)(\w*)([\\\=]*)(?|(\")\"&quot;\"|)(?|(.*)?&quot;(\")|)([\ ]?)(\/|)&gt;/i";
	$replacement = "<$1$2$3$4$5$6$7$8$9$10>";
	$text = preg_replace($tags, $replacement, $text);
	$text = preg_replace("/=\"\"/", "=", $text);
	$text = strip_tags($text, ''); // remove extra tags

	return $text;
}


/**
 * Update the table are necessary <br>
 * When the program starts, the
 *
 * @param array $ent array of entries
 */
function updateTable(array $ent): void{
	if (!empty($ent)){
		echo "<table><tr><th class='th20'>FRÅN</th><th class='th40'>INLÄGG</th>";
		echo "<th class='th40'>LOGGNING</th></tr>";
		foreach ($ent as $entry){
			echo "<tr>";
			foreach ($entry as $value){
				echo "<td>";
				if (is_array($value)){
					echo "IP:   " . sanitize($value["IP"]) . "<br>TID: "
					     . sanitize($value["TID"]);
				}else{
					echo sanitize($value);
				}
				echo "</td>";
			}
			echo "</tr>";
		}
		echo "</table>";
	}else{
		echo "<p>Fill in the guess book below</p>";
	}
}

/**
 * Check if this is the same person trying to make an entry
 *
 * @return bool True if same IP or same computer
 */
function isSamePerson(): bool{
	if (isset($_COOKIE["IP"]) && ($_COOKIE["IP"] === $_SERVER["REMOTE_ADDR"])){
		return true;
	}

	return false;
}


/**
 * Authenticate a user against the array of users and their passwords
 *
 * @param string $username the username of the user
 * @param string $password the user's password
 * @param array  $allUsers the array of all users in the system
 *
 * @return array an array with errors or an empty array if user credentials are
 *               ok
 */
function authenticateUser(
	string $username,
	string $password,
	array $allUsers
): array{
	$feedback = [];
	if (in_array($username, $allUsers)){
		if ($allUsers[$username] === $password){
			return $feedback;
		}else{
			$feedback["password_mismatch"]
				= "The password does not match with the username provided";
		}
	}else{
		$feedback["user_absent"]
			= "Username '$username' does not exist in our system";
	}

	return $feedback;
}

/**
 * Display the links of a particular page.
 *
 * @param array $links the array of links (key-pair)
 */
function displayLinks(array $links){
	if (!empty($links)){
		echo "<nav id='links'> <ul>";
		foreach ($links as $key => $value){
			echo "<li><a href='$value'>" . ucfirst($key) . "</a></li>";
		}
		echo "</ul></nav>";
	}
}

/**
 * Escape string before sql processing
 *
 * @param string $string
 *
 * @return string
 */
function escapestring(string $string): string{
	return pg_escape_string($string);
}


/**
 * Display the messages from the database. If no message is returned, do not
 * display any table
 *
 * @param array $messages messages read from the database
 */
function printMessagesFromDb(array $messages){
	if (empty($messages)){
		echo "<p>Please fill in the guest book</p>";
	}else{
		echo "<table><tr><th class='th20'>FRÅN</th>"
		     . "<th class='th40'>INLÄGG</th>" .
		     "<th class='th40'>LOGGNING</th></tr>";
		foreach ($messages as $message){
			echo "<tr><td>" . $message->getName() . "</td>"
			     . "<td>" . $message->getText() . "</td>"
			     . "<td>IP: " . $message->getIp() . "<br>TID: " . $message->getTime()
			     . "</td></tr>";
		}
		echo "</table>";
	}

}