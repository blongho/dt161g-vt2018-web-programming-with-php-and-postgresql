<?PHP
session_start();
include_once "app/functions.php";
/*******************************************************************************
 * Laboration 3, Kurs: DT161G
 * File: login.php
 * Desc: Login page for laboration 2
 *
 * Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 ******************************************************************************/

// User_array holds username and password
// There are two users m with password m and a whit password a
$user_array = [
	"m" => "m",
	"a" => "a",
	"b" => "b",
];
/** This array holds the links to be displayed when a user has logged in
 * */
$link_array = [
	"Hem"         => "index.php",
	"Gästbok"     => "guestbook.php",
	"Medlemssida" => "members.php",
];

/**
 * Declare a variable to hold the amount of time that a remember me cookie will
 * be set
 */
define("SIX_MONTHS", 6 * 30 * 24 * 3600);

$username = "";
$password = "";
$usernameHasValue = false;
$passwordHasValue = false;
$rememberMe = false;
$feedback = []; // array to hold the feedback information
$isSuccessfulLogin = false;
$authenticate = [];

if (strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {

	// process user name and set errors if there exist
	if (isset($_POST["username"])) {
		$username = $_POST["username"];
		if ( ! empty($username)) {
			$username = sanitize($username);
			$usernameHasValue = true;
		} else {
			$feedback["userNameEmpty"] = "User name cannot be empty";
		}
	} else {
		$feedback["userNameNotSet"] = "Could not process username value";
	}

	// Process password and set errors is there exist
	if (isset($_POST["password"])) {
		$password = $_POST["password"];
		if ( ! empty($password)) {
			$password = sanitize($password);
			$passwordHasValue = true;
		} else {
			$feedback["passwordEmpty"] = "Password cannot be empty";
		}
	} else {
		$feedback["passwordNotSet"] = "Could not process the password value";
	}

	// do stuff if password and username has some value
	if ($passwordHasValue && $usernameHasValue) {
		$authenticate = authenticateUser($username, $password, $user_array);
		if (empty($authenticate)) {
			$_SESSION["user"] = $username;
			$_SESSION["loggedIn"] = true;
			$_SESSION["links"] = $link_array;
			$feedback["links"] = $link_array;
			$feedback["loggedIn"] = $_SESSION["loggedIn"];
			$feedback["successMessage"]
				= "You successfully logged in as '$username'";
			$isSuccessfulLogin = true;
		}
	} else {
		$_SESSION["loggedIn"] = false;
		$feedback["success"] = false;
	}

	$feedback = array_merge($feedback, $authenticate);

	header('Content-Type: application/json; charset=utf-8');
	echo json_encode($feedback);
} else { // nothing posted, redirect to home page
	header("Location: index.php");
}


