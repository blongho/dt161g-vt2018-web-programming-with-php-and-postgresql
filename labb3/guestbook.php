<?PHP
session_start();
include_once "util.php";

/*******************************************************************************
 * Laboration 3, Kurs: DT161G
 * File: guestbook.php
 * Desc: Guestbook page for laboration 3 from lab2
 *
 * Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 ******************************************************************************/
$title = "Laboration 3";
$userName = "";
$today = "";
$userText = "";
$captchaError = "";
$userIP = $_SERVER['REMOTE_ADDR'];
$errors = [];
$links = [
	"Hem" => "index.php",
];

$isUserLoggedIn = false;
if (isset($_SESSION["user"])) {
	$links = $_SESSION["links"];
	$isUserLoggedIn = true;
}
$captchaValues = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

/**
 *  initial session captcha
 */
if ( ! isset($_POST["submit"])) {
	$_SESSION["captcha"] = substr(str_shuffle($captchaValues), 0, 5);
}

/**
 * Generate new captcha if the user entered the right one
 * If wrong captcha value is entered, update $errors array with a message
 * depicting that and generate a new captcha
 * */
if (isset($_POST["submit"])) {
	if ($_POST["captcha"] == $_SESSION["captcha"]) {
		$_SESSION["captcha"] = substr(str_shuffle($captchaValues), 0, 5);
	} else {
		if ($_POST["captcha"] != $_SESSION["captcha"]) {
			$errors["wrongCaptcha"] = "Wrong value for captcha";
			$_SESSION["captcha"] = substr(str_shuffle($captchaValues), 0, 5);
		}
	}
}

$submitted = isset($_POST["submit"]);

/**
 * Placeholder for input area
 */
$placeholder = isset($_POST["text"]) ? $_POST["text"]
	: "Skriv ditt inlägg här...";

/**
 * When the user submits the form, the name, text, ip address is extracted as
 * well as the date.
 * If the captcha value displayed is entered correctly, a cookie is set with
 * the user's IP address, a post item is created, the values of the post are
 * added to that of the field which in turns updates the table. This updated
 * values are then saved.
 */

if (isset($_POST["submit"])) {
	$userName = $_POST["name"];
	$userText = $_POST["text"];

	if ($userName !== "" && $userText !== "" && $userIP !== "") {
		$userName = sanitize($userName);
		$userText = sanitize($userText);
		if (empty($errors)) {
			$post = new Message($userName, $userText);
			$saveMessage = DbManager::postMessage($post);
			if (isset($saveMessage["success"])) {
				setcookie("IP", $userIP, time() + 60 * 60 * 24 * 30);
				header("Refresh:0");
			}
		}
	}

}
$messages = DbManager::getAllMessages();
/*******************************************************************************
 * HTML section starts here
 ******************************************************************************/
?>
<!DOCTYPE html>
<html lang="sv-SE">
<!-- Latest file as of 2018-08-06 -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css"/>
    <title>DT161G-Laboration 3</title>
    <script src="js/main.js"></script>
</head>
<body>
<header>
    <img src="img/mittuniversitetet.jpg" alt="miun logga" class="logo"/>
    <h1><?php echo $title; ?></h1>
</header>
<main>
    <aside>
        <div id="authenticate">
            <div id="login">
				<?php if ( ! $isUserLoggedIn): ?>
                <h2>LOGIN</h2>
                <form id="loginForm">
                    <label for="uname">Username
                        <input type="text" placeholder="m" name="uname"
                               id="uname"
                               maxlength="10" value="m" autocomplete="off">
                    </label>
                    <label for="psw">Password
                        <input type="password" placeholder="Enter Password"
                               name="psw"
                               id="psw"></label>
                    <label for="loginButton">
                        <button type="button" id="loginButton" name="login">
                            Login
                        </button>
                    </label>
                </form>
            </div>
            <div id="logout">
				<?php else: ?>
                    <h2>LOGOUT</h2>
                    <button type="button" id="logoutButton" name="logout">
                        Logout
                    </button>
				<?php endif; ?>
            </div>
        </div>
        <h2>MENY</h2>
		<?php displayLinks($links); ?>
        <div id="authenticationFeedback">
            <p id="success" class="green hide"></p>
            <p id="usernameError" class="hide red"></p>
            <p id="passwordError" class="hide red"></p>
        </div>
    </aside>
    <section>
        <h2>GÄSTBOK</h2>
		<?php printMessagesFromDb($messages); ?>
		<?php if ( ! isSamePerson()): ?>
            <form action="guestbook.php" method="POST">
                <fieldset>
                    <legend>Skriv i gästboken</legend>
                    <label>Från:
                        <input type="text" placeholder="Skriv ditt namn"
                               name="name" value="<?php echo isset
						(
							$_POST["name"]
						) ? $_POST["name"] : "" ?>">
                    </label>
                    <label for="text">Inlägg</label>
                    <textarea id="text" name="text"
                              rows="10" cols="50"
                              placeholder="<?php echo
					          $placeholder ?>"></textarea>
                    <label>Captcha: <span
                                class="red"><?php echo $_SESSION["captcha"];
							?></span>
                        <input type="text" placeholder="Skriva captcha här"
                               name="captcha" required> </label>
                    <button type="submit"
                            name="submit" id="postMessage">Skicka
                    </button>
                    <p id="captchaError"> <span class="red"><?php
							if (isset($errors["emptyCaptcha"])) {
								echo $errors["emptyCaptcha"];
							}
							if (isset($errors["wrongCaptcha"])) {
								echo $errors["wrongCaptcha"];
							} ?>
                        </span></p>
                </fieldset>
            </form>
		<?php else: ?>
            <p>Thank you for filling our guess book.</p>
		<?php endif ?>

    </section>
</main>
<footer>
	<?php printFooter(); ?>
</footer>
</body>
</html>
