<?php
include_once "util.php";
/**
 * Created by PhpStorm.
 * Initialize the Database.
 *
 * NOTE: Run this file only once or any time that the intention is to clear the database and
 * start afresh
 *
 * User: Bernard Che Longho (lobe1602)
 * Date: 8/10/2018
 * Time: 3:12 PM
 */

$initializeDb
	= "DROP SCHEMA IF EXISTS dt161g CASCADE;


CREATE SCHEMA dt161g;

DROP TABLE IF EXISTS dt161g.guestbook;

CREATE TABLE dt161g.guestbook (
  id        SERIAL PRIMARY KEY,
  name      text      NOT NULL CHECK (name <> ''),
  message   text      NOT NULL CHECK (message  <> ''),
  iplog     inet      NOT NULL CHECK (iplog <> inet '0.0.0.0'),
  timelog   timestamp DEFAULT now()
)
WITHOUT OIDS;

INSERT INTO dt161g.guestbook (name, message, iplog, timelog) VALUES ('test','testing','192.168.1.1', '2018-02-02 12:21:21');";

$db = Database::getInstance();
$db->doSimpleQuery($initializeDb);

