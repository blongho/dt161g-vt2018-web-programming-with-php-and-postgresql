<?php
//include_once "../util.php";

/*******************************************************************************
 * Laboration 3, Kurs: DT161G
 * File: dbmanager.class.php
 * Desc: Class DbManager for laboration 3.
 * This class manages communication with the database, all updates on the
 * database are done here Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 ******************************************************************************/
class DbManager{

	private static $DB_TABLE = "dt161g.guestbook";

	/**
	 * DbManager constructor is empty. Let the heavy lifting be done by the
	 * respective functions
	 */
	public function __construct(){
	}

	/**
	 * Post a message to the database
	 *
	 * @param Message $msg the message
	 *
	 * @return array an array with key-value messages. Keys failed|success as the case might be
	 */
	public static function postMessage(Message $msg): array{
		$feedback = [];
		if ($msg !== null){
			$name = escapestring($msg->getName());
			$query = "INSERT INTO " . self::$DB_TABLE
			         . " ( name, message, iplog, timelog) "
			         . "VALUES ($1, $2, $3, $4);";

			$db = Database::getInstance();

			$results = $db->doParamQuery($query, (array) $msg);

			if ($results !== null){
				$feedback["success"]
					= "Thank you <b>'$name'</b> for filling our guess book";
				pg_free_result($results);

			}else{
				$feedback["failed"]
					= "Dear <b>'$name'</b>, your message could not be posted";
			}

		}else{
			$feedback["failed"] = "Attempt to post empty message refused!";
		}

		return $feedback;
	}

	/**
	 * Get all messages in the database
	 *
	 * @return array all database messages
	 */
	public static function getAllMessages(): array{
		$query = "SELECT * FROM " . self::$DB_TABLE .
		         " ORDER BY timelog ASC;";
		$db = Database::getInstance();
		$results = $db->doSimpleQuery($query);
		$messages = [];
		if (!empty($results)){
			while ($msg = pg_fetch_row($results)){
				$messages [] = new Message($msg[1], $msg[2], $msg[3], $msg[4]);
			}
			pg_free_result($results);

		}

		return $messages;
	}
}