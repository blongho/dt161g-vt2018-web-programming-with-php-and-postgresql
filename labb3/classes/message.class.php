<?php

/*******************************************************************************
 * Laboration 3, Kurs: DT161G
 * File: message.class.php
 * Desc: Class Message for laboration 3.
 * This is class represents a message
 *
 * Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 *
 * @since 2018-08-06
 ******************************************************************************/
class Message
{

	private $name = "";
	private $text = "";
	private $ip = "";
	private $time = "";

	/**
	 * Message constructor.
	 *
	 * @param string $name the writer's name
	 * @param string $text the text entered
	 * @param null   $ip
	 * @param null   $time
	 */
	public function __construct(
		string $name,
		string $text,
		$ip = null,
		$time = null
	) {
		$this->name = sanitize($name);
		$this->text = sanitize($text);
		$this->ip = sanitize(($ip === null ? $_SERVER["REMOTE_ADDR"] : $ip));
		$this->time = sanitize(
			($time === null ? date("Y-m-d H:i", time()) : $time)
		);
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getText(): string
	{
		return $this->text;
	}

	/**
	 * @param string $text
	 */
	public function setText(string $text): void
	{
		$this->text = $text;
	}

	/**
	 * @return string the ip address
	 */
	public function getIp(): string
	{
		return $this->ip;
	}

	/**
	 * @param string $ip
	 */
	public function setIp(string $ip): void
	{
		$this->ip = $ip;
	}

	/**
	 * @return null
	 */
	public function getTime()
	{
		return $this->time;
	}

	/**
	 * @param string $time
	 */
	public function setTime(string $time): void
	{
		$this->time = $time;
	}

	/**
	 * Display table on table row
	 */
	public function displayAsRow(): void
	{
		echo "<tr><td>$this->name</td>"
		     ."<td>$this->text</td>"
		     ."<td>IP: $this->ip<br>TID: $this->time</td></tr>";
	}


	public function showMessage(): void
	{
		echo "Name: $this->name<br>Text: $this->text<br>IP: $this->ip<br>Time: $this->time<br>";
	}

	function updateTable(array $ent): void
	{
		if ( ! empty($ent)) {
			echo "<table><tr><th class='th20'>FRÅN</th><th class='th40'>INLÄGG</th>";
			echo "<th class='th40'>LOGGNING</th></tr>";
			foreach ($ent as $entry) {
				echo "<tr>";
				foreach ($entry as $value) {
					echo "<td>";
					if (is_array($value)) {
						echo "IP:   ".$value["IP"]."<br>TID: ".$value["TID"];
					} else {
						echo sanitize($value);
					}
					echo "</td>";
				}
				echo "</tr>";
			}
			echo "</table>";
		} else {
			echo "<p>Fill in the guess book below</p>";
		}
	}
}