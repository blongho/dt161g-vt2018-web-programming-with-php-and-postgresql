<?php
/**
 * @file functions.php
 * Created by PhpStorm.
 * User: lobe1602, Bernard Che Longho
 * Date: 8/2/2018
 * Time: 8:10 PM
 * This file contains some global functions that will be used across the project
 */

/**
 * Print the footer of the page
 */
function printFooter(){
	echo "<p>Brought to you by <a href='mailto:lobe1602@student.miun.se'>Bernard Che Longho</a></p>";
}

/**
 * Remove all special characters that could be submitted in a form
 *
 * @see Source: http://php.net/manual/en/function.htmlspecialchars.php#101592
 *
 * @param string $text text to be sanitized the form
 *
 * @return null|string|string[] formatted text with no tags
 */
function sanitize(string $text): string{
	$text = htmlspecialchars($text);
	$text = preg_replace("/=/", "=\"\"", $text);
	$text = preg_replace("/&quot;/", "&quot;\"", $text);
	$tags
		= "/&lt;(\/|)(\w*)(\ |)(\w*)([\\\=]*)(?|(\")\"&quot;\"|)(?|(.*)?&quot;(\")|)([\ ]?)(\/|)&gt;/i";
	$replacement = "<$1$2$3$4$5$6$7$8$9$10>";
	$text = preg_replace($tags, $replacement, $text);
	$text = preg_replace("/=\"\"/", "=", $text);
	$text = strip_tags($text, ''); // remove extra tags

	return $text;
}


/**
 * Update the table are necessary <br>
 * When the program starts, the
 *
 * @param array $entries array of entries
 */
function updateTable(array $entries){
	if (!empty($entries)){
		echo "<table><tr><th class='th20'>FRÅN</th><th class='th40'>INLÄGG</th>";
		echo "<th class='th40'>LOGGNING</th></tr>";
		foreach ($entries as $entry){
			echo "<tr>";
			foreach ($entry as $value){
				echo "<td>";
				if (is_array($value)){
					echo "IP:   " . sanitize($value["IP"]) . "<br>TID: "
					     . sanitize($value["TID"]);
				}else{
					echo sanitize($value);
				}
				echo "</td>";
			}
			echo "</tr>";
		}
		echo "</table>";
	}else{
		echo "<p>Fill in the guess book below</p>";
	}
}

/**
 * Check if this is the same person trying to make an entry
 *
 * @return bool True if same IP or same computer
 */
function isSamePerson(): bool{
	if (isset($_COOKIE["IP"]) && ($_COOKIE["IP"] === $_SERVER["REMOTE_ADDR"])){
		return true;
	}

	return false;
}

/**
 * Get the path to the entries file
 *
 * @return string relative path to the json file
 */
function pathToFile(): string{

	return "../../writeable/data.json";  // path to file
}
