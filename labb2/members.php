<?php
session_start();
include_once "app/functions.php";
/*******************************************************************************
 * Laboration 2, Kurs: DT161G
 * File: member.php
 * Desc: Member page for laboration 2. Only logged in users are authorized to access this page
 * <p>Unauthenticated users are redirected to index.php
 *
 * Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 ******************************************************************************/
$title = "Laboration 2";
// Här skall det ske kontroll om man har loggat in och är behörig att se denna sida.
// Annars redirekt till startsidan

$isUserLoggedIn = false;
$links = array();
if (!isset($_SESSION["user"])){
	header("Location: index.php"); /* Redirect browser */
	exit;
}else{
	$title = "Laboration 2";
	$user = $_SESSION["user"];
	$links = $_SESSION["links"];
	$isUserLoggedIn = true;
}
?>

<!DOCTYPE html>
<html lang="sv-SE">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DT161G-Laboration2-member</title>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="js/main.js"></script>
</head>
<body>
<header>
    <img src="img/mittuniversitetet.jpg" alt="miun logga" class="logo"/>
    <h1><?php echo $title; ?></h1>
</header>
<main>
    <aside>
        <div id="authenticate">
            <div id="login">
				<?php if (!$isUserLoggedIn): ?>
                <h2>LOGIN</h2>
                <form id="loginForm">
                    <label for="uname">Username
                        <input type="text" placeholder="m" name="uname" id="uname"
                               maxlength="10" value="m" autocomplete="off">
                    </label>
                    <label for="psw">Password
                        <input type="password" placeholder="Enter Password" name="psw"
                               id="psw"></label>
                    <label for="loginButton">
                        <button type="button" id="loginButton" name="login">Login</button>
                    </label>
                </form>
            </div>
            <div id="logout">
				<?php else: ?>
                    <h2>LOGOUT</h2>
                    <button type="button" id="logoutButton" name="logout">Logout</button>
				<?php endif; ?>
            </div>
        </div>
        <h2>MENY</h2>
		<?php displayLinks($links); ?>
        <div id="authenticationFeedback">
            <p id="success" class="green hide"></p>
            <p id="usernameError" class="hide red"></p>
            <p id="passwordError" class="hide red"></p>
        </div>
    </aside>
    <section>
        <h2>Medlemssida</h2>
        <p>Denna sida skall bara kunna ses av inloggade medlemmar</p>
		<?php if ($isUserLoggedIn): ?>
            <p>You are logged in as '<b><?php echo $_SESSION["user"]; ?></b>'</p>
		<?php endif; ?>
    </section>
</main>
<footer>
	<?php printFooter(); ?>
</footer>
</body>
</html>

