<?PHP
include_once "app/functions.php";
session_start();

/*******************************************************************************
 * Laboration 2, Kurs: DT161G
 * File: index.php
 * Desc: Start page for laboration 2
 *
 * Bernard Che Longho (lobe1602)
 * lobe1602@student.miun.se
 ******************************************************************************/
$title = "Laboration 2";

$links = array(
	"Hem"     => "index.php",
	"Gästbok" => "guestbook.php",
);
$isUserLoggedIn = false;
if (isset($_SESSION["user"])){
	$user = $_SESSION["user"];
	$links = $_SESSION["links"];
	$isUserLoggedIn = true;
}
/*******************************************************************************
 * HTML section starts here
 ******************************************************************************/
?>
<!DOCTYPE html>
<html lang="sv-SE">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DT161G-<?php echo $title ?></title>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="js/main.js"></script>
</head>
<body>
<header>
    <img src="img/mittuniversitetet.jpg" alt="miun logga" class="logo"/>
    <h1><?php echo $title ?></h1>
</header>
<main>
    <aside>
        <div id="authenticate">
            <div id="login">
				<?php if (!$isUserLoggedIn): ?>
                <h2>LOGIN</h2>
                <form id="loginForm">
                    <label for="uname">Username
                        <input type="text" placeholder="m" name="uname" id="uname"
                               maxlength="10" value="m" autocomplete="off">
                    </label>
                    <label for="psw">Password
                        <input type="password" placeholder="Enter Password" name="psw"
                               id="psw"></label>
                    <label for="loginButton">
                        <button type="button" id="loginButton" name="login">Login</button>
                    </label>
                </form>
            </div>
            <div id="logout">
				<?php else: ?>
                    <h2>LOGOUT</h2>
                    <button type="button" id="logoutButton" name="logout">Logout</button>
				<?php endif; ?>
            </div>
        </div>
        <h2>MENY</h2>
		<?php displayLinks($links); ?>
    </aside>
    <section>
        <h2>VÄLKOMMEN
        </h2>
        <p>Detta är andra laborationen</p>
		<?php if ($isUserLoggedIn): ?>
            <p>You are logged in as '<b><?php echo $_SESSION["user"]; ?></b>'</p>
		<?php endif; ?>
        <div id="authenticationFeedback">
            <p id="success" class="green hide"></p>
            <p id="usernameError" class="hide red"></p>
            <p id="passwordError" class="hide red"></p>
        </div>
    </section>
</main>
<footer>
	<?php printFooter(); ?>
</footer>
</body>
</html>
